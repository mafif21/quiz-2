var app = new Vue({
  el: "#app",
  data: {
    users: [
      {
        name: "Muhammad Iqbal Mubarok",
      },
      {
        name: "Ruby Purwanti",
      },
      {
        name: "Faqih Muhammad",
      },
    ],
    masukan: "",
  },
  computed: {},
  methods: {
    addUser: function () {
      this.users.push({ name: this.masukan });
      this.masukan = "";
    },

    deleteUser: function () {
      this.users.shift();
    },
  },
});
